# Logitech Pro Wireless battery charge notification

Add to Menu > Preferences > Startup Applications


**Discharging notification**

![discharging notification](discharging.png)

**Charging notification**

![Charging notification](charging.png)
